# Subroutines By Shreyas

Tech blog site by [Shreyas](https://shreyasgokhale.com)

Theme template: [Gesko](https://github.com/P0WEX) by [@P0WEX](https://github.com/P0WEX/Gesko). Thanks for the awesome theme!

## License

This project is open source and available under the [MIT License](LICENSE.md).
