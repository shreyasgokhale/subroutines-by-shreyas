---
title: "Creating your own website - Difficulty Level: 0"
tags: website no-code
description: Your first step into creating an online presence.
header:
  ? image
  ? caption
  ? teaser
minute: 10
---
# Your first step into creating an online presence

Before we jump in, just go to <https://sites.google.com/view/shreyasgokhale/home>

Did you like it? Would you like to make one for yourself?

I'll let you in on a secret. It took me 15 minutes and $0 to make this website. 

And by the end of this blog, you will too!

(Now you might find some strange similarities between this site and an episode of a "popular" Netflix series, but I assure you, there is absolutely no relation!)

## Why to even have a website?

In a world where everything has an app for it, why bother creating a new website? Job recruiters use LinkedIn, influencers use instagram and bloggers write on medium. Let's just use that.

Yes but all those apps are created by *someone.* And the *someone* is not *you*! These apps usually get profited from your data and they prioritize the people that make *them* the most money. Are these platforms great to begin with? Sure! But as soon as you want to have a definite place to put your career achievements, or want to have a place for a blog with the design you like, you need to set up your own website.  

And the most important part: it's fun! You will learn something new! And at times like these, web development skills go a long way! Plus, if you haven't done programming before, this is the best way to get into it! At the time of writing this, the world is in a lockdown due to COVID-19, and making a website might be one of the best use of your home quarantine!

## I am in! Tell me more!

I must confess something before we go ahead, I am not a web developer! When I decided that I wanted to develop, everything was so confusing. It took me some time to figure out stuff. But don't worry, I'll try to make your journey easy. 

Let's understand different parts of a website. First, there is an address: [google.com](http://google.com) , [amazon.de](http://amazon.de) , <https://www.india.gov.in/topics>.  Did you notice the variations in these addresses? The "google" in google.com is called as a domain. It is the name of your website. Second, there is this `.com` `.de` etc. It is termed as TLD or Top Level Domain. The last one is a complete URL, with the protocol https which links to the page "topics" inside `india.gov.in`. These domains are sold by a registrar. They act as a directory service for your website. If you want a custom domain like [shreyasgokhale.com](http://shreyasgokhale.com) , you will have to buy it from these providers.

The second part is the actual website itself. There are a lot of technologies involved in making the webpage but the core one is html. 

Now if you want to avoid all the hassle of learning these web technologies, there are services called as website builders. They include options like [wix.com](http://wix.com), [wordpress.com](http://wordpress.com) which offer an easy drag-and-drop way to create a nice looking website. They work just like creating presentations. You can customize some options, get themes and even publish it. But beyond some basic stuff, everything is paid. 

The second type of websites is something like [Jekyll](https://jekyllrb.com/). We can build nice static websites using just simple `yaml` and `markdown` format. This whole website ([shreyasgokhale.com](http://www.shreyasgokhale.com)) has been made with the same technology. It is not as simple as drag-and-drop, but it is very easy to set up. It is a hybrid between a full fledged web development. 

And finally, typical web development setup: HTML, CSS, JavaScript and then corresponding package managers (Like NPM) , frameworks (bootstrap) and technologies. My resume website ([https://resume.shreyasgokhale.com/](http://resume.shreyasgokhale.com)) has been created using such a technology. 

Now once you have finalized why you want to have, we can move on. Let's start with one of the easiest ways to create websites: Google Sites. Yes, you heard that right. Google site has been around for a while and it is simple, yet quite powerful. With a few clicks, you will have a website in no time! Don't worry, I'll also create a site with you. As you know, I already have one resume website and one blog website. Now, I want to create a simple webpage where one can select which website to choose. So, head to https://sites.google.com/ and log in with your Google account.

Click on the "+" , or select from the template. 

![](/assets/images/website-blog-screenshot1.png)



On the right side, you can see options to choose different layouts, add various elements and change themes. 

![](/assets/images/screenshot-from-2022-01-23-13-35-46.png)

I selected a side by side layout to present the visitor with a choice. Add some images and text, just like you do in a power point presentation. Click on the preview to see how your website will look like.

And just like that, you have created your first website! Yay!

In the next blog, we will look at some other aspects of website and how we can improve our current website.