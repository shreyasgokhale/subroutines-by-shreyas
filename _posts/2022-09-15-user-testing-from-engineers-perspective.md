---
title: Engineer’s perspective in user testing
minute: 3
tags: []
description: Recently I got a chance to visit our live user testing in Nette.
  Here are my top 4 impressions from the engineer’s perspective
---
# Engineer’s perspective in user testing

Recently I got a chance to visit our live user testing in Nette. Here are my top 4 impressions from the engineer’s perspective

## User testing helps in Identifying and fixing bugs early

Testing in production is nothing to be afraid about, [even Netflix advocates it](https://launchdarkly.com/blog/testing-in-production-the-netflix-way/). However, when your product directly impacts the day to day of your user, it is better to do it in a safe environment. User testing could be one of the best ways to finding nasty bugs early. 

For example, we discovered a bug which was very hard to spot during regression testing.  But, looking at the interaction, we already had a suspicion how this could be happening, and we patched it immediately after the interview! Normally, it could have taken at least some time for Product alignment → Bug ticket → Prioritization → Bug fixing cycle.  

## Discovering new ways to implement features / create new product/s

Usually, product team aligns with the tech team to create new features and then the engineers find a way to make it happen. However sometimes the context / inspiration behind such feature is lost in the translation. When engineers are also a part of the user testing, they experience the situation firsthand, and engineers can already start thinking about possible solutions and improvements to the product.

## User testing is the best way to get feedback on the product that you have been working on

Engineers work very closely with some features, and we always think that they could be done in some better way. However, it is important to get an opinion from users outside your company.  Also, it feels great when someone appreciates a feature, and it is you who has built it.

Many users gave kudos on our blind details screen and the UX. Users were also very happy about the switches. And there are so many other moments which brought smile to my face during the testing. 

## Appreciating the complex task of interviewing and contributing back for the tech tips

I had never been a part of the user test. But after this, I have newfound appreciation for the interviewers and note takers. It takes a huge effort to pull it off and to make it a perfect experience for the user. However, sometimes there can be some small creases or bugs around the corners, which we know we are going to fix in the future. 

Our user research was a great success and some tech tips from engineers and developers might have helped to make it seamless ;)